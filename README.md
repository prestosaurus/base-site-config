Creates our base paragraph type and all base/generic fields to be reused in other paragraph types. Do NOT build content with this entity.

**Paragraph type**:

 - base_paragraph_fields

**Paragraph fields**:

 - &lt;a&gt; Multiple
 - &lt;a&gt; Single
 - &lt;abbr&gt;
 - &lt;address&gt;
 - &lt;caption&gt;
 - &lt;cite&gt;
 - &lt;em&gt;
 - &lt;figcaption&gt;
 - &lt;figure&gt;
 - &lt;h2&gt;
 - &lt;h3&gt;
 - &lt;h4&gt;
 - &lt;h5&gt;
 - &lt;h6&gt;
 - &lt;img&gt; Multiple
 - &lt;img&gt; Single
 - &lt;li&gt;
 - &lt;option&gt;
 - &lt;p&gt;
 - &lt;s&gt;
 - &lt;source&gt;
 - &lt;sub&gt;
 - &lt;summary&gt;
 - &lt;sup&gt;
 - Boolean
 - Date
 - Date and Time
 - File
 - Float Option Select
 - Full HTML
 - Paragraph Reference
 - Views Reference
 - WebForm Reference
 - Yes/No Select
 
**Premise**:

Create all field bases in this paragraph type, and reuse them in other paragraph types used for display.
