<?php

namespace Drupal\custom_helper_services\Services;

use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;

/**
 * Class CustomHelperServices.
 *
 * Provides helper services to be used in modules and themes.
 */
class CustomHelperServices {

  /**
   * Entity Type Manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Route Match Interface.
   *
   * @var Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Entity Repository Interface.
   *
   * @var Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * CustomHelperServices constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The Route Match Interface.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The Entity Repository Interface.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RouteMatchInterface $routeMatch, EntityRepositoryInterface $entityRepository) {
    $this->entityTypeManager = $entityTypeManager;
    $this->routeMatch = $routeMatch;
    $this->entityRepository = $entityRepository;
  }

  /**
   * Build our image style path service.
   */
  public function getFieldImage($field = NULL, $imageStyle = NULL) {

    if ($field->entity) {

      // Get image file URI.
      $uri = $field->entity->getFileUri();

      // Build image style path.
      $url = $this->entityTypeManager
        ->getStorage('image_style')
        ->load($imageStyle)
        ->buildUrl($uri);

      // Get image attributes.
      $alt = $field->alt;
      $width = $field->width;
      $height = $field->height;

      // Handle empty alt text.
      $quotes = ['"' , "'"];
      $nothing = ["" , ""];
      if (strtolower(str_replace($quotes, $nothing, $alt)) === "empty") {
        $alt = "";
      }

      return [
        'url' => $url,
        'alt' => $alt,
        'width' => $width,
        'height' => $height,
      ];
    }

    return NULL;
  }

  /**
   * Build our media style path service.
   */
  public function getMediaFieldImage($mediaField = NULL, $imageStyle = NULL) {

    if ($mediaField && $mediaField->entity) {

      // Get image.
      $mediaImage = $mediaField->entity
        ->get('field_media_image');

      // Get image attributes.
      $alt = $mediaImage->alt;
      $width = $mediaImage->width;
      $height = $mediaImage->height;
      $title = $mediaImage->title;

      // Handle empty alt text.
      $quotes = ['"' , "'"];
      $nothing = ["" , ""];
      if (strtolower(str_replace($quotes, $nothing, $alt)) === "empty") {
        $alt = "";
      }

      // Get image entity.
      $mediaImageEntity = $mediaImage->entity;

      if ($mediaImageEntity) {

        // Get image mime type.
        $mediaImageMimeType = $mediaImageEntity->getMimeType();

        // Get image file URI.
        $mediaImageEntityUri = $mediaImageEntity->getFileUri();

        // Set image URL.
        $url = file_create_url($mediaImageEntityUri);

        // Set image style URL.
        if ($imageStyle && $mediaImageMimeType !== "image/svg+xml") {
          $url = $this->entityTypeManager
            ->getStorage('image_style')
            ->load($imageStyle)
            ->buildUrl($mediaImageEntityUri);
        }

        return [
          'url' => $url,
          'alt' => $alt,
          'width' => $width,
          'height' => $height,
          'mimeType' => $mediaImageMimeType,
          'title' => $title,
        ];
      }
    }

    return NULL;
  }

  /**
   * Build our media file service.
   */
  public function getMediaFieldFile($mediaField = NULL) {

    if ($mediaField && $mediaField->entity) {

      // Get entity.
      $mediaEntity = $mediaField->entity;

      // Get file.
      $mediaFile = $mediaField->entity
        ->get('field_media_file');

      // Set name.
      $mediaName = $mediaEntity->getName();

      // Set description.
      $mediaDescription = NULL;
      if ($mediaEntity->hasField('field_description') && $mediaEntity->get('field_description')->value != NULL) {
        $mediaDescription = $mediaEntity->get('field_description')->value;
      }

      // Get file entity.
      $mediaFileEntity = $mediaFile->entity;

      // Get file name.
      $mediaFileName = $mediaFileEntity->getFileName();

      // Get file size.
      $mediaFileSize = $mediaFileEntity->getSize();

      // Get file mime type.
      $mediaFileMimeType = $mediaFileEntity->getMimeType();

      // Get file file URI.
      $mediaFileEntityUri = $mediaFileEntity->getFileUri();

      // Set file URL.
      $url = file_url_transform_relative(file_create_url($mediaFileEntityUri));

      return [
        'url' => $url,
        'name' => $mediaName,
        'description' => $mediaDescription,
        'fileName' => $mediaFileName,
        'fileSize' => $mediaFileSize,
        'mimeType' => $mediaFileMimeType,
      ];
    }

    return NULL;
  }

  /**
   * Build our media files service.
   */
  public function getMediaFieldFileMultiple($mediaField = NULL) {

    if ($mediaField && $mediaField->entity) {
      $returnArray = [];

      foreach ($mediaField as $field) {

        // Get entity.
        $mediaEntity = $field->entity;

        // Get file.
        $mediaFile = $field->entity
          ->get('field_media_file');

        // Set name.
        $mediaName = $mediaEntity->getName();

        // Set description.
        $mediaDescription = NULL;
        if ($mediaEntity->hasField('field_description') && $mediaEntity->get('field_description')->value != NULL) {
          $mediaDescription = $mediaEntity->get('field_description')->value;
        }

        // Get file entity.
        $mediaFileEntity = $mediaFile->entity;

        // Get file name.
        $mediaFileName = $mediaFileEntity->getFileName();

        // Get file size.
        $mediaFileSize = $mediaFileEntity->getSize();

        // Get file mime type.
        $mediaFileMimeType = $mediaFileEntity->getMimeType();

        // Get file file URI.
        $mediaFileEntityUri = $mediaFileEntity->getFileUri();

        // Set file URL.
        $url = file_url_transform_relative(file_create_url($mediaFileEntityUri));

        $returnArray[] = [
          'url' => $url,
          'name' => $mediaName,
          'description' => $mediaDescription,
          'fileName' => $mediaFileName,
          'fileSize' => $mediaFileSize,
          'mimeType' => $mediaFileMimeType,
        ];
      }

      return $returnArray;
    }

    return NULL;
  }

  /**
   * Get a media entity by uuid.
   */
  public function getMediaByUuid($uuid = NULL, $viewMode = "default") {

    $uuidIsValid = Uuid::isValid($uuid);

    if ($uuidIsValid) {
      $mediaEntity = $this->entityRepository
        ->loadEntityByUuid('media', $uuid);
      $mediaEntityView = $this->entityTypeManager
        ->getViewBuilder('media')
        ->view($mediaEntity, $viewMode);

      return $mediaEntityView;
    }

    return NULL;
  }

  /**
   * Get node.
   */
  public function getNode($variables = NULL) {

    $routeName = $this->routeMatch->getRouteName();

    // $variables['node'].
    if (isset($variables['node'])) {
      if (is_numeric($variables['node'])) {
        return $this->entityTypeManager->getStorage('node')->load($variables['node']);
      }
      return $variables['node'];
    }

    // $variables['row'].
    if (isset($variables['row']) && !empty($variables['row']->nid)) {
      return $this->entityTypeManager->getStorage('node')->load($variables['row']->nid);
    }

    // $routeName === 'entity.node.canonical'.
    if ($routeName === 'entity.node.canonical') {
      return $this->routeMatch->getParameter('node');
    }

    // $routeName === 'entity.node.revision'.
    if ($routeName === 'entity.node.revision') {
      $revisionId = $this->routeMatch->getParameter('node_revision');
      return node_revision_load($revisionId);
    }

    // $routeName === 'entity.node.preview'.
    if ($routeName === 'entity.node.preview') {
      return $this->routeMatch->getParameter('node_preview');
    }

    return NULL;
  }

}
