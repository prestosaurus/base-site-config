# Base Image Styles

This module provides config for common image styles.

## Image style options:

 - (35mm Film 96dpi) 91 x 136
 - (35mm Film 96dpi) 136 x 91
 - (35mm Film 300dpi) 283 x 425
 - (4x6 96dpi) 384 x 576
 - (35mm Film 300dpi) 425 x 283
 - (16x9) 480 x 270
 - (5x7 96dpi) 480 x 672
 - (A5 96dpi) 559 x 794
 - (6x4 96dpi) 576 x 384
 - (7x5 96dpi) 672 x 480
 - (16x9) 800 x 450
 - 800 x 480
 - (4x6 300dpi) 1200 x 1800
 - 1440px
 - Circle image styles (sizes from 10px to 1400px. Adds div wrapper for css border-radius with HOOK_preprocess_image_style().)
 - (16x9) 1440 x 810
 - (5x7 300dpi) 1500 x 2100
 - (A5 300dpi) 1748 x 2480
 - (6x4 300dpi) 1800 x 1200
 - (7x5 300dpi) 2100 x 1500
 - (iPad Pro 9.7, Air 2, Mini 4) 1536 x 2048
 - (iPad Pro 10.5) 1668 x 2224
 - (iPad Pro 12.6) 2048 x 2732
 - (iPhone 6, 6s, 7, 8 native) 750 x 1334
 - (iPhone 6 Plus, 6s Plus, 7 Plus, 8 Plus native) 1080 x 1920
 - (iPhone 6E native) 640 x 1136
 - (iPhone X native) 1125 x 2436
 - Teaser Image 290 x 163