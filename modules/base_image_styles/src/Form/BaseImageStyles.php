<?php

namespace Drupal\base_image_styles\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Config\FileStorage;
use Symfony\Component\Yaml\Yaml;
use Drupal\image\Entity\ImageStyle;

/**
 * Defines a form for the base image styles module.
 */
class BaseImageStyles extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'base_image_styles.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get form config.
    $config = $this->config('base_image_styles.settings');

    // Set $config_path.
    $module_path = drupal_get_path('module', 'base_image_styles');
    $config_path = $module_path . '/config/optional';

    // Instantiate $configLabelList.
    $configLabelList = [];

    // Cycle through files in config directory.
    if (is_dir($config_path)) {
      if ($dh = opendir($config_path)) {
        while (($fileName = readdir($dh)) !== FALSE) {
          if (pathinfo($fileName, PATHINFO_EXTENSION) == 'yml') {

            // Parse YAML file.
            $ymlValues = Yaml::parse(file_get_contents($config_path . '/' . $fileName));

            // Build image style label list.
            $configLabelList[] = $ymlValues['label'];
          }
        }
        closedir($dh);
      }
    }

    // Sort array naturally readable.
    natcasesort($configLabelList);

    $form['#tree'] = TRUE;

    // General information.
    $form['bis_file_name_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => 'Manage Base Image Styles',
    ];

    $form['bis_file_name_wrapper']['information'] = [
      '#type' => 'item',
      '#title' => '<p>List of image style config files in this module.</p>',
    ];

    // Image style options.
    $form['bis_file_name_wrapper']['set_image_styles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Config File Name'),
      '#required' => TRUE,
      '#options' => $configLabelList,
      '#default_value' => $config->get('set_image_styles') != NULL ? $config->get('set_image_styles') : 0,
    ];

    $form['bis_file_name_wrapper']['details'] = [
      '#type' => 'item',
      '#title' => '<p>Checking an option above installs that config. Unchecking an option above deletes that config.</p>',
    ];

    // Apply button.
    $form['bis_apply_config_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => 'Apply Selections Above',
    ];

    $form['bis_apply_config_wrapper']['apply_config'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#submit' => ['::configureImageStyles'],
    ];

    return $form;
  }

  public function configureImageStyles(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    // Instantiate $configFileList.
    $configFileList = [];

    // Instantiate $configLabelList.
    $configLabelList = [];

    // Set $config_path.
    $module_path = drupal_get_path('module', 'base_image_styles');
    $config_path = $module_path . '/config/optional';

    // Cycle through files in config directory.
    if (is_dir($config_path)) {
      if ($dh = opendir($config_path)) {
        while (($fileName = readdir($dh)) !== FALSE) {
          if (pathinfo($fileName, PATHINFO_EXTENSION) == 'yml') {

            // Build image style list, remove file extension.
            $configFileList[] = str_replace('.yml', '', $fileName);

            // Parse YAML file.
            $ymlValues = Yaml::parse(file_get_contents($config_path . '/' . $fileName));

            // Build image style label list.
            $configLabelList[] = $ymlValues['label'];
          }
        }
        closedir($dh);
      }
    }

    // Install/delete config.
    foreach ($values['bis_file_name_wrapper']['set_image_styles'] as $key => $value) {

      // Check that config exists and should be installed.
      if ($value > 0
        && ImageStyle::load(str_replace('image.style.', '', $configFileList[$key])) == NULL
      ) {

        // Install selected image style config.
        $source = new FileStorage($config_path);
        $config_storage = \Drupal::service('config.storage');
        $config_storage
          ->write($configFileList[$key], $source->read($configFileList[$key]));

        // Add image style name to Drupal message.
        \Drupal::messenger()
          ->addMessage('Installed: ' . $configLabelList[$key], MessengerInterface::TYPE_STATUS);
      }

      // Check that config exists and should be deleted.
      elseif ($value == 0
        && ImageStyle::load(str_replace('image.style.', '', $configFileList[$key])) != NULL
      ) {

        // Delete selected image style config.
        \Drupal::configFactory()
          ->getEditable($configFileList[$key])->delete();

        // Add image style name to Drupal message.
        \Drupal::messenger()
          ->addMessage('Deleted: ' . $configLabelList[$key], MessengerInterface::TYPE_WARNING);
      }
    }

    // Set form values.
    $this->configFactory->getEditable('base_image_styles.settings')
      ->set('set_image_styles', $values['bis_file_name_wrapper']['set_image_styles'])
      ->save();

  }

}
